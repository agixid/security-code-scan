package main

import (
	"bufio"
	"os"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func init() {
	os.Setenv("CI_PROJECT_DIR", "/tmp/app")
}

var testInput = `Microsoft (R) Build Engine version 15.7.177.53362 for .NET Core
Copyright (C) Microsoft Corporation. All rights reserved.

  Restore completed in 45.11 ms for /tmp/app/app.csproj.
WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]
  app -> /tmp/app/bin/Debug/netcoreapp2.0/app.dll

Build succeeded.

WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]
    1 Warning(s)
    0 Error(s)

Time Elapsed 00:00:01.69`

func TestMatch(t *testing.T) {
	want := []string{
		"WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]",
		"WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]",
	}
	scanner := bufio.NewScanner(strings.NewReader(testInput))
	got := []string{}
	for scanner.Scan() {
		line := scanner.Text()
		if match(line) {
			got = append(got, line)
		}
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Expected match to be\n%#v\nbut got\n%#v", want, got)
	}
}

func TestParse(t *testing.T) {
	want := &SecWarning{
		SourceFile:  "WeakRandom.cs",
		Line:        7,
		Column:      16,
		RuleID:      "SCS0005",
		Message:     "Weak random generator",
		ProjectFile: "/tmp/app/app.csproj",
	}
	got, _ := parse("WeakRandom.cs(7,16): warning SCS0005: Weak random generator [/tmp/app/app.csproj]")
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Expected\n%#v\nbut got\n%#v", want, got)
	}
}

func TestConvert(t *testing.T) {
	r := strings.NewReader(testInput)

	want := &issue.Report{
		Version: issue.CurrentVersion(),
		Vulnerabilities: []issue.Issue{
			{
				Category: "sast",
				Scanner: issue.Scanner{
					ID:   scannerID,
					Name: scannerName,
				},
				Name:    "Weak random generator",
				Message: "Weak random generator",
				Location: issue.Location{
					File:      "WeakRandom.cs",
					LineStart: 7,
				},
				Identifiers: []issue.Identifier{
					{
						Type:  "security_code_scan_rule_id",
						Name:  "SCS0005",
						Value: "SCS0005",
						URL:   "https://security-code-scan.github.io/#SCS0005",
					},
				},
				CompareKey: "WeakRandom.cs:7:SCS0005",
			},
		},
		Remediations: []issue.Remediation{},
	}
	got, err := convert(r, "app")
	if err != nil {
		t.Fatal(err)
	}
	if !reflect.DeepEqual(want, got) {
		t.Errorf("Wrong result. Expected:\n%#v\nbut got:\n%#v", want, got)
	}
}
