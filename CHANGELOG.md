# security-code-scan .net analyzer changelog

## v2.0.3
- Print stderr in case of execution error (!12)

## v2.0.2
- Bump common to v2.1.6

## v2.0.1
- Bump common to v2.1.5

## v2.0.0
- Switch to new report syntax with `version` field

## v1.3.0
- Upgrade to .NET SDK 2.2

## v1.2.0
- Add `Scanner` property and deprecate `Tool`

## v1.1.0
- Show command error output

## v1.0.0
- initial release
