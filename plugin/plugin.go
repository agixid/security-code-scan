package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

func Match(path string, info os.FileInfo) (bool, error) {
	switch filepath.Ext(info.Name()) {
	case ".csproj", ".vbproj":
		return true, nil
	default:
		return false, nil
	}
}

func init() {
	plugin.Register("security-code-scan", Match)
}
